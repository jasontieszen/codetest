describe("Global functions",()=> {
    describe("Circle", ()=> {
       it("should return the area", ()=> {
           const circle = Circle(2);

           expect(+circle.getArea().toFixed(2)).toEqual(12.57)
        });
        it("should return toString value", ()=> {
            const circle = Circle(2);

            expect(circle.toString()).toEqual("Circle: Radius=2, Area = 12.566370614359172");
        });

    });

    describe("Square", ()=> {
        it("should return the area", ()=> {
            const square = Square(8);

            expect(square.getArea()).toEqual(64)
        });
        it("should return toString value", ()=> {
            const square = Square(8);

            expect(square.toString()).toEqual("Square: Size=8, Area = 64");
        });
    });

    describe("ShapeBuilder", ()=> {
        let shapeBuilder;
        beforeEach(()=>{
            shapeBuilder = ShapeBuilder(Circle, Square);
        });
        describe("shortShapes", ()=>{
            it("should order shapes in descending area", ()=>{
                const shapes = [Circle(1), Square(1), Circle(2), Square(2), Circle(3), Square(3), Circle(4), Square(4)];

                const sortedShapes = shapeBuilder.shortShapes(shapes);
                const sortedAreas = sortedShapes.map((x)=>{
                    return x.getArea();
                });

                expect(sortedAreas).toEqual([ 50.26548245743669, 28.274333882308138, 16, 12.566370614359172, 9, 4, 3.141592653589793, 1 ]);
            });
        });

        describe("generateShapes", ()=>{
            it("should generate 50 squares and 50 circles by default", ()=>{
                const generatedShapes = shapeBuilder.generateShapes();

                const circles = generatedShapes.filter((x)=>{
                    return x.shapeType === 'circle';
                });
                const squares = generatedShapes.filter((x)=>{
                    return x.shapeType === 'square';
                });

                expect(circles.length).toEqual(50);
                expect(squares.length).toEqual(50);
            });

            it("should have all circles with a diameter between 1 and 100", ()=>{
                const generatedShapes = shapeBuilder.generateShapes(1000);

                const circles = generatedShapes.filter((x)=>{

                    return x.shapeType === 'circle' && x.size >= 1 && x.size <= 100;
                });
                expect(circles.length).toEqual(1000);
            });

            it("should have all squares with a size between 1 and 100", ()=>{
                const generatedShapes = shapeBuilder.generateShapes(1000);

                const squares = generatedShapes.filter((x)=>{
                    return x.shapeType === 'square' && x.size >= 1 && x.size <= 100;
                });

                expect(squares.length).toEqual(1000);
            });

        });
    });




});
