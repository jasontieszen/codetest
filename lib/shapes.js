"use strict";

function ShapeBuilder(circle, square){
    const shapeBuilder =  {
        square : square,
        circle : circle,
        autoRedraw : autoRedraw,
        drawShapes : drawShapes,
        shortShapes : shortShapes,
        generateShapes : generateShapes
    };

    function shortShapes(shapes){
        return shapes.sort((shape1, shape2) =>{
            return shape2.getArea() - shape1.getArea();
        });
    }

    function generateShapes(count){
        let shapeCount = count || 50;

        function generateShape(shape, min, max){
            let shapes = [];
            for(let i=0; i<shapeCount; i++){
                shapes.push(shape(random(min, max)));
            }
            return shapes;
        }

        function random(min, max){
            return min + Math.floor(Math.random() * (max - min +1));
        }

        return generateShape(circle, 1, 50)
            .concat(generateShape(square, 1, 100));
    }

    function drawShapes(querySelector){
        const fragment = document.createDocumentFragment();
        shortShapes(generateShapes()).forEach((shape) => {
            const element = document.createElement("div");
            _setShapeAttributes(element, shape);
            fragment.appendChild(element)
        });
        const container = document.querySelector(querySelector);
        container.classList.add("shapeContainer");
        container.appendChild(fragment);
    }

    function redrawShapes(querySelector, deg){
        const container = document.querySelector(querySelector);
        const domElements = container.children;
        shortShapes(generateShapes()).forEach((shape, index) =>{
            const element = domElements[index];
            _setShapeAttributes(element, shape, deg);
        });
    }

    function autoRedraw(querySelector){
        let deg = 10;
        drawShapes('#shapeContainer');
        window.setInterval(()=>{
            redrawShapes(querySelector, deg);
            deg = deg === 360 ? 10 : deg + 10;
        }, 500);
    }

    function _setShapeAttributes(element, shape, deg){
        element.className = "shapeContainer__shape";
        element.setAttribute("style",`height:${shape.size}px;width:${shape.size}px;`);
        element.setAttribute("title", shape.toString());

        if(shape.shapeType === "circle"){
            element.classList.add("shapeContainer__shape--circle");
        }
        if(shape.shapeType === "square"){
            element.classList.add("shapeContainer__shape--square");
            if(deg){
                element.style.transform=`rotate(${deg}deg)`;
            }
        }
    }

    return shapeBuilder;
}

function Circle(radius){
    const circle = {
        shapeType: "circle",
        radius : radius,
        size : radius * 2,
        getArea: getArea,
        toString: toString
    };

    function getArea(){
        return Math.PI * Math.pow(radius, 2);
    }
    function toString(){
        return `Circle: Radius=${radius}, Area = ${getArea()}`;
    }
    return circle;
}

function Square(size){
    const square = {
        shapeType : "square",
        size: size,
        getArea: getArea,
        toString: toString
    };
    function getArea(){
        return Math.pow(size, 2);
    }
    function toString(){
        return `Square: Size=${size}, Area = ${getArea()}`;
    }
    return square;
}



