const gulp = require('gulp');
const jasmineBrowser = require('gulp-jasmine-browser');
const watch = require('gulp-watch');

gulp.task('default', function() {
    const files = ['lib/**/*.js', 'spec/**/*spec.js'];
    return gulp.src(files)
        .pipe(watch(files))
        .pipe(jasmineBrowser.specRunner())
        .pipe(jasmineBrowser.server({port: 8888}));
});